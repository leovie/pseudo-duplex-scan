from PyPDF2 import PdfFileWriter, PdfFileReader, PdfFileMerger
import os
from random import randint
import sys

# def calculateIndizes(pageCount):
    # new = dict()
    # for i in range (1, int(pageCount / 2) + 1, 1):
        # oldIndex = i
        # newIndex = (2 * oldIndex) - 1
        # new[oldIndex] = newIndex
        
    # for i in range (int(pageCount / 2) - 1, -1, -1):
        # oldIndex = pageCount - i
        # newIndex = oldIndex - i
        # new[oldIndex] = newIndex
        
    # return new
    
def calculateIndizes(pageCount):
    new = dict()
    for i in range (1, int(pageCount / 2) + 1, 1):
        oldIndex = i
        newIndex = (2 * oldIndex) - 1
        new[oldIndex] = newIndex
        
    for i in range (int(pageCount / 2) - 1, -1, -1):
        oldIndex = pageCount - i
        newIndex = pageCount - (oldIndex - i) + 2
        new[oldIndex] = newIndex
        
    return new

inputpdf = PdfFileReader(open(sys.argv[1], "rb"))
newIndizes = calculateIndizes(inputpdf.numPages)
filenames = dict()

rand = randint(1000000, 9999999)
for i in range(inputpdf.numPages):
    output = PdfFileWriter()
    output.addPage(inputpdf.getPage(i))
    filename = "./tmp/" + str(rand) + "_" + str(newIndizes[i+1]) + ".pdf"
    filenames[newIndizes[i+1]] = filename

    with open(filename, "wb") as outputStream:
        output.write(outputStream)
        

merger = PdfFileMerger()
for index, filename in sorted(filenames.items()):
    merger.append(PdfFileReader(open(filename, 'rb')))

merger.write("document-output.pdf")
